package subscriber

import (
	"encoding/json"
	"errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/streadway/amqp"
	"notification-service/dto"
	"notification-service/utils"
)


func HandleTaskDeadline(d amqp.Delivery) error {
	var msg dto.EventTaskMessage
	if err := json.Unmarshal(d.Body, &msg); err != nil {
		return errors.New("invalid message")
	}
	emailClient := utils.Email{Message: "Your task " + msg.Name + " must be completed before " + msg.EndDate + ", please finish the task and change the task status!", Subject: "[EMS] Remind: Task Deadline", FromEmail: "tungpdhe130238@fpt.edu.vn", ToEmail: msg.Email}
	err := emailClient.SendMailFromEmail()
	if err != nil {
		log.Info(err.Error())
		return errors.New("error while sending email")
	}
	return nil
}

func HandleNewTask(d amqp.Delivery) error {
	var msg dto.EventTaskMessage
	if err := json.Unmarshal(d.Body, &msg); err != nil {
		return errors.New("invalid message")
	}
	emailClient := utils.Email{Message: "You have been assigned to a new task which will be due in " + msg.EndDate + ", please open EMS application and check it!", Subject: "[EMS] New task for you", FromEmail: "tungpdhe130238@fpt.edu.vn", ToEmail: msg.Email}
	err := emailClient.SendMailFromEmail()
	if err != nil {
		log.Info(err.Error())
		return errors.New("error while sending email")
	}
	return nil
}

func HandleNewApplication(d amqp.Delivery) error {
	var msg dto.ApplicationMessage
	if err := json.Unmarshal(d.Body, &msg); err != nil {
		return errors.New("invalid message")
	}
	emailClient := utils.Email{Message: "You have a new application waiting for your approval, please go to EMS website and check it out!",
		Subject: "[EMS] New Application", FromEmail: "tungpdhe130238@fpt.edu.vn", ToEmail: msg.Email}
	err := emailClient.SendMailFromEmail()
	if err != nil {
		log.Info(err.Error())
		return errors.New("error while sending email")
	}
	return nil
}

func HandleApplicationApproval(d amqp.Delivery) error {
	var msg dto.ApplicationMessage
	if err := json.Unmarshal(d.Body, &msg); err != nil {
		return errors.New("invalid message")
	}
	emailClient := utils.Email{Message: "Your application \""+msg.Name +"\" has been " + msg.Status + "!",
		Subject: "[EMS] Application Approval", FromEmail: "tungpdhe130238@fpt.edu.vn", ToEmail: msg.Email}
	err := emailClient.SendMailFromEmail()
	if err != nil {
		log.Info(err.Error())
		return errors.New("error while sending email")
	}
	return nil
}

//func Handler(ctx context.Context, msg *notificationservice.Message) error {
//	log.Info("Function Received message: ", msg.Say)
//	return nil
//}
