# NotificationService Service

This is the NotificationService service

Generated with

```
micro new --namespace=go.micro --type=service notification-service
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: go.micro.service.notification-service
- Type: service
- Alias: notification-service

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend etcd.

```
# install etcd
brew install etcd

# run etcd
etcd
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./notification-service-service
```

Build a docker image
```
make docker
```