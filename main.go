package main

import (
	"ems/shared/plugins/broker/rabbitmq"
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
	"notification-service/subscriber"
)

func main() {

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.notification"),
		micro.Version("latest"),
	)

	// Initialise service

	service.Init()
	_, err := rabbitmq.NewConsumer("task_deadline_notification", "go.micro.service.notification", subscriber.HandleTaskDeadline)
	if err != nil {
		log.Fatalf("%s", err)
	}
	_, err = rabbitmq.NewConsumer("task_assign_notification", "go.micro.service.notification", subscriber.HandleNewTask)
	if err != nil {
		log.Fatalf("%s", err)
	}
	_, err = rabbitmq.NewConsumer("application_approval_notification", "go.micro.service.notification", subscriber.HandleApplicationApproval)
	if err != nil {
		log.Fatalf("%s", err)
	}
	_, err = rabbitmq.NewConsumer("new_application_notification", "go.micro.service.notification", subscriber.HandleNewApplication)
	if err != nil {
		log.Fatalf("%s", err)
	}
	// Register Handler
	//notificationservice.RegisterNotificationServiceHandler(service.Server(), new(handler.NotificationService))

	// Register Struct as Subscriber
	if err != nil {
		log.Fatal(err)
	}

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
