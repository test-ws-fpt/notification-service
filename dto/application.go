package dto


type ApplicationMessage struct {
	Name string
	Status string
	Email string
}

