package dto

type EventTaskMessage struct {
	Name string		`json:"name"`
	EndDate string	`json:"end_date"`
	Email string	`json:"email"`
}
